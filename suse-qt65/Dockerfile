FROM opensuse/tumbleweed
MAINTAINER openSUSE KDE Maintainers <opensuse-kde@opensuse.org>

# Add KDE:Qt6 repo
ARG OBS_REPO=KDE:Qt6
RUN zypper --non-interactive addrepo --priority 50 --refresh obs://${OBS_REPO}/openSUSE_Tumbleweed ${OBS_REPO}
# Update container, import GPG key for KDE:Qt repo
RUN zypper --non-interactive --gpg-auto-import-keys -v dup
# CUPS-devel at least does not like Busybox versions of some things, so ensure it is not used
# Likewise, we have packages that do not want LibreSSL so lock it out too
RUN zypper al busybox busybox-gzip libressl-devel
# Install various other packages
RUN zypper --non-interactive install java-1_8_0-openjdk-headless python3-lxml python3-paramiko python3-PyYAML python3-simplejson wget file tar gzip go rsync
# ffmpegthumbs, needs to be before devel_qt or the ffmpeg-5-mini-libs package will be installed with no codec at all
RUN zypper --non-interactive install ffmpeg-5-libavcodec-devel ffmpeg-5-libavfilter-devel ffmpeg-5-libavformat-devel ffmpeg-5-libavdevice-devel ffmpeg-5-libavutil-devel ffmpeg-5-libswscale-devel ffmpeg-5-libpostproc-devel

# Install build dependencies
RUN zypper --non-interactive install --recommends -t pattern devel_C_C++
# The pattern is likely not enough, so just install all Qt devel packages from KDE:Qt6
RUN zypper -q se --not-installed-only --repo ${OBS_REPO} qt6*devel | tail -n +4 | cut -d "|" -f 2 | xargs zypper --non-interactive in
# And some other useful and base packages
RUN zypper --non-interactive in git git-lfs clang python3-atspi python3-Sphinx xvfb-run AppStream python3-pip ruby-devel libffi-devel openbox sassc ccache ninja meson \
    # dependency of appstream-glib
    libyaml-devel libstemmer-devel \
    # temporarily: curl needed for appstreamcli, cmp. https://bugzilla.opensuse.org/show_bug.cgi?id=1080446
    curl \
    # basic Qt6 packages, which have no -devel and should be manually installed
    qt6-declarative-tools \
    qt6-qt5compat-imports \
    qt6-networkinformation-nm \
    qt6-multimedia-imports \
    qt6-location \
    qt6-positioning-imports \
    qt6-connectivity \
    # Other basic Qt based libraries
    qca-qt6-devel \
    # For building documentation tarballs
    bzip2 \
    # For image thumbnails for the KDE.org/applications subsite
    ImageMagick \
    # Hidden dependency of appstream tools
    gsettings-desktop-schemas \
    # Useful tools for static analysis
    clazy cppcheck \
    # Needed for API Documentation generation
    python3-gv graphviz-gd qt6-tools-helpgenerators \
    # Needed for some unit tests to function correctly
    hicolor-icon-theme \
    # Needed for some projects that use the non-standard catch2 Unittest mechanisms.
    Catch2-devel \
    # Needed by KDE Connect on X11
    libfakekey-devel
# Install components needed for the CI tooling to operate (python-gitlab, gcovr, cppcheck_codequality) as well as other CI jobs (check-jsonschema)
# as well as reuse (for unit tests), doxyqml (for building QML documentation) and cheroot/wsgidav/ftpd (for KIO unit tests)
# We also bring in chai and pygdbmi which is used by DrKonqi unit tests
RUN pip install --break-system-packages python-gitlab gcovr cppcheck_codequality reuse doxyqml cheroot wsgidav check-jsonschema chai pygdbmi
RUN gem install ftpd
# KDE stuff also depends on the following
RUN zypper --non-interactive in --allow-vendor-change \
    # kdesrc-build
    perl-JSON perl-YAML-LibYAML perl-IO-Socket-SSL \
    # modemmanager-qt
    ModemManager-devel \
    # networkmanager-qt
    NetworkManager-devel \
    # kcoreaddons
    lsof \
    # kauth
    polkit-devel \
    # kwindowsystem
    xcb-*-devel \
    # karchive
    libzstd-devel \
    # prison
    libdmtx-devel qrencode-devel \
    # kimageformats
    openexr-devel libavif-devel libheif-devel \
    # kwayland and kwin
    wayland-devel \
    wayland-protocols-devel \
    libdisplay-info-devel \
    # baloo/kfilemetadata (some for okular)
    libattr-devel libexiv2-devel libtag-devel taglib-*-devel libepub-devel libpoppler-qt6-devel lmdb-devel \
    # kdoctools
    perl-URI docbook_4 docbook-xsl-stylesheets libxml2-devel libxslt-devel perl-URI \
    # kio
    libacl-devel libmount-devel libblkid-devel \
    # various projects need OpenSSL
    libopenssl-devel \
    # kdnssd
    libavahi-devel libavahi-glib-devel libavahi-gobject-devel \
    # khelpcenter and pim
    libxapian-devel \
    # sonnet
    aspell \
    aspell-devel \
    hunspell-devel \
    libvoikko-devel \
    # kio-extras and krdc, kio-fuse
    libssh-devel fuse3-devel libseccomp-devel djvulibre \
    # plasma-pa
    libpulse-devel libcanberra-devel \
    # user-manager
    libpwquality-devel \
    # sddm-kcm
    libXcursor-devel \
    # plasma-workspace
    libXtst-devel \
    xdotool \
    # breeze-plymouth
    plymouth-devel \
    # kde-gtk-config/breeze-gtk
    gtk3-devel gtk2-devel python3-cairo \
    # plasma-desktop/discover
    itstool \
    #libAppStreamQt-devel \ TODO not available for Qt6 yet
    PackageKit PackageKit-devel \
    packagekitqt6-devel \
    fwupd-devel \
    # plasma-desktop
    xf86-input-synaptics-devel xf86-input-evdev-devel xf86-input-libinput-devel libxkbfile-devel xorg-x11-server-sdk xdg-user-dirs \
    # kimpanel
    ibus-devel scim-devel \
    # libksane
    sane-backends-devel \
    # pim
    libical-devel libkolabxml-devel libxerces-c-devel \
    # <misc>
    alsa-devel libraw-devel fftw3-devel adobe-sourcecodepro-fonts \
    # choqok
    qtkeychain-qt6-devel \
    # krita
    eigen3-devel OpenColorIO-devel dejavu-fonts gnu-free-fonts libraqm-devel libunibreak-devel \
    quazip-qt6-devel \
    # kaccounts / telepathy
    # TODO not available for Qt6 yet
    # libaccounts-qt5-devel
    libaccounts-glib-devel \
    # TODO not available for Qt6 yet
    # libsignon-qt5-devel
    intltool \
    # skrooge
    sqlcipher sqlcipher-devel sqlite3-devel sqlite3 libofx-devel poppler-tools \
    # kwin
    libepoxy-devel Mesa-demo Mesa-demo-x xorg-x11-server-extra dmz-icon-theme-cursors libgbm-devel weston \
    xorg-x11-server-wayland \
    # kgamma5
    libXxf86vm-devel \
    # kgraphviewer
    graphviz-devel \
    # drkonqi
    at-spi2-core which libgirepository-1_0-1 typelib-1_0-Atspi-2_0 gobject-introspection-devel \
    # kcalc
    mpfr-devel \
    # kdevelop
    gdb \
    # labplot
    gsl-devel \
    # kalzium
    # avogadrolibs-devel \ TODO pulls in Qt5 
    openbabel-devel \
    ocaml-facile-devel \
    # kuserfeedback
    php8 \
    # digikam
    # QtAV-devel \ TODO not available for Qt6 yet / pulls in Qt5
    opencv-devel exiftool \
    # wacomtablet
    libwacom-devel \
    xf86-input-wacom-devel \
    # rust-qt-binding-generator
    rust rust-std \
    cargo \
    # kdevelop
    clang \
    clang-devel \
    llvm-devel \
    #subversion-devel \ TODO causes package conflicts?
    python3-devel \
    # clazy
    clang-devel-static \
    # libkleo
    libqgpgmeqt6-devel \
    # akonadi
    mariadb qt6-sql-mysql \
    # libkdegames
    openal-soft-devel \
    libsndfile-devel \
    # kscd
    libmusicbrainz5-devel \
    # audiocd-kio
    cdparanoia-devel \
    # ark
    libarchive-devel libzip-devel \
    # k3b
    flac-devel \
    libmad-devel \
    libmp3lame-devel \
    libogg-devel libvorbis-devel \
    libsamplerate-devel \
    # kamera
    libgphoto2-devel \
    # signon-kwallet-extension
    # signond-libs-devel \ TODO pulls in Qt5
    # kdenlive
    libmlt-devel \
    libmlt7-data \
    libmlt7-module-qt6 \
    melt \
    rttr-devel \
    # print-manager
    cups-devel \
    # krfb
    LibVNCServer-devel \
    # kscd
    libdiscid-devel \
    # minuet
    fluidsynth-devel \
    # kajongg
    python3-Twisted \
    # okular
    texlive-latex libdjvulibre-devel libmarkdown-devel chmlib-devel \
    # ksmtp tests
    cyrus-sasl-plain \
    # kdb
    libmariadb-devel postgresql-devel \
    # Gwenview
    cfitsio-devel \
    # Calligra, Krita and probably other things elsewhere too
    libboost_*-devel \
    # Amarok
    gmock gtest libcurl-devel libofa-devel libgpod-devel libmtp-devel loudmouth-devel libmysqld-devel \
    # liblastfm-qt5-devel TODO not available for qt6 yet
    # Cantor
    libspectre-devel \
    python3-numpy \
    python3-matplotlib \
    octave \
    maxima \
    libqalculate-devel \
    # julia-devel \ ### package no longer provided by OpenSUSE
    # KPat
    freecell-solver-devel black-hole-solver-devel \
    # RKWard
    R-base-devel gcc-fortran \
    # Kaffeine
    libdvbv5-devel \
    # vlc-devel \ TODO pulls in Qt5
    libXss-devel \
    # Keysmith
    libsodium-devel \
    # Plasma Phone Components
    libphonenumber-devel \
    # kquickcharts
    glslang-devel \
    # xdg-desktop-portal-kde
    pipewire pipewire-devel \
    # Spectacle
    # kImageAnnotator-devel kColorPicker-devel \ TODO Qt5 based
    # upnp-lib-qt
    kdsoap-qt6-devel \
    # KSysGuard
    libnl3-devel \
    # Kjournald
    systemd-devel systemd-journal-remote \
    # Smb4k
    libsmbclient-devel \
    # ksystemstats
    libsensors4-devel \
    # kitinerary, qrca
    zxing-cpp-devel \
    # ki18n
    iso-codes-devel \
    iso-codes-lang \
    # Neochat
    qcoro-qt6-devel \
    libQuotient-qt6-devel \
    cmark cmark-devel \
    # KWave
    audiofile-devel id3lib-devel \
    # elf-dissector
    libdwarf-devel \
    # trojita
    libmimetic-devel \
    # plasma-pass
    liboath-devel \
    # Krita
    Vc-devel libmypaint-devel libheif-devel openjpeg2-devel \
    # Skanpage
    tesseract-ocr-devel leptonica-devel \
    # kup
    libgit2-devel \
    # plasma-nm
    mobile-broadband-provider-info \
    # Spacebar
    c-ares-devel \
    # kxstitch
    ImageMagick-devel \
    libMagick++-devel \
    # plasma-dialer (kde-telephony-daemon)
    callaudiod-devel \
    # discover, flatpak-kcm
    flatpak-devel \
    # kmymoney
    aqbanking-devel \
    # xdg-portal-test-kde
    gstreamer-devel \
    # Haruna
    mpv-devel \
    # kscreenlocker
    libpamtest-devel \
    # NeoChat
    olm-devel \
    # Sink/kube
    flatbuffers-devel

# Temporary appstream-glib with flatpak patches.
RUN git clone https://github.com/hughsie/appstream-glib -b appstream_glib_0_8_2 && cd appstream-glib && git clone https://invent.kde.org/sysadmin/ci-images && git apply ./ci-images/suse-qt65/appstream-patches/appstream-glib-000* && meson setup build --prefix=/usr -Drpm=false -Dgtk-doc=false && ninja -C build && ninja -C build install && cd .. && rm -rf appstream-glib

# Temporary libappstream until it's released with Qt 6 support
RUN git clone https://github.com/ximion/appstream -b main && cd appstream && git checkout 8f93b38 && meson setup build --prefix=/usr -Dqt=true && ninja -C build && ninja -C build install && cd .. && rm -rf appstream

# For D-Bus to be willing to start it needs a Machine ID
RUN dbus-uuidgen > /etc/machine-id
# Certain X11 based software is very particular about permissions and ownership around /tmp/.X11-unix/ so ensure this is right
RUN mkdir /tmp/.X11-unix/ && chown root:root /tmp/.X11-unix/ && chmod 1777 /tmp/.X11-unix/
# We need a user account to do things as, and we need specific group memberships to be able to access video/render DRM nodes
RUN groupadd -g 44 host-video && groupadd -g 109 host-render && useradd -d /home/user/ -u 1000 --user-group --create-home -G video,host-video,host-render --shell /usr/bin/bash user

# wireplumber does not start because its logind module fails. Temporary workaround until wireplumber 0.5
RUN rm /usr/lib64/wireplumber-0.4/libwireplumber-module-logind.so

# Switch to our unprivileged user account
USER user
