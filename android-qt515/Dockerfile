FROM ubuntu:22.04

ARG NDK_VERSION=22.1.7171670
ARG SDK_PLATFORM=android-33
ARG SDK_BUILD_TOOLS=30.0.3
ARG MIN_NDK_PLATFORM=android-21
ARG SDK_PACKAGES="tools platform-tools"

ENV DEBIAN_FRONTEND noninteractive
ENV ANDROID_HOME /opt/android-sdk
ENV ANDROID_SDK_ROOT ${ANDROID_HOME}
ENV ANDROID_NDK_ROOT /opt/android-sdk/ndk/${NDK_VERSION}
ENV ANDROID_NDK_HOST linux-x86_64
ENV ANDROID_NDK_PLATFORM ${MIN_NDK_PLATFORM}
ENV ANDROID_API_VERSION ${SDK_PLATFORM}
ENV ANDROID_BUILD_TOOLS_REVISION ${SDK_BUILD_TOOLS}
ENV PATH ${ANDROID_HOME}/cmdline-tools/tools/bin:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools:${PATH}
ENV ANDROID_NDK $ANDROID_NDK_ROOT
ENV STANDALONE_EXTRA="--stl=libc++"

RUN apt update && apt full-upgrade -y && apt install -y --no-install-recommends \
    unzip \
    git \
    openssh-client \
    ca-certificates \
    locales \
    sudo \
    curl \
    make \
    rdfind \
    autoconf \
    libtool \
    autotools-dev \
    automake \
    ninja-build \
    pkg-config \
    openjdk-11-jdk \
    build-essential \
    gettext \
    gperf \
    bison \
    flex \
    nasm \
    shared-mime-info \
    p7zip \
    p7zip-full \
    p7zip-rar \
    zstd \
    python3 \
    python3-pip \
    python3-venv \
    python3-distutils \
    python3-future \
    python3-xdg \
    python3-requests \
    python3-yaml \
    python3-lxml \
    python3-paramiko \
# There are some (build) dependencies of Craft blueprints that don't (cross) compile for Android, but on the host.
# Craft at this point isn't set up to produce both host and target binaries, so we need host tools in the image.
    autopoint \
    && apt-get -qq clean \
    && locale-gen en_US.UTF-8 && dpkg-reconfigure locales \
    && update-alternatives --install /usr/bin/python python /usr/bin/python3 10

# Install components needed by Gitlab CI
RUN pip install python-gitlab packaging

##########################

RUN chmod a+w /opt/

# Add group & user
RUN groupadd -r user && useradd --create-home --gid user user && echo 'user ALL=NOPASSWD: ALL' > /etc/sudoers.d/user

USER user
WORKDIR /home/user
ENV HOME /home/user

##########################

# Download & unpack android SDK
ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk-amd64
RUN curl -Lo /tmp/sdk-tools.zip 'https://dl.google.com/android/repository/commandlinetools-linux-6609375_latest.zip' \
    && mkdir -p /opt/android-sdk/cmdline-tools && unzip -q /tmp/sdk-tools.zip -d /opt/android-sdk/cmdline-tools && rm -f /tmp/sdk-tools.zip \
    && yes | sdkmanager --licenses && sdkmanager --verbose "platforms;${SDK_PLATFORM}" "build-tools;${SDK_BUILD_TOOLS}" "ndk;${NDK_VERSION}" ${SDK_PACKAGES} && sdkmanager --uninstall --verbose emulator

# compile native tooling
RUN mkdir /opt/nativetooling

RUN mkdir -p /opt/cmake \
    && curl -Lo /tmp/cmake.sh https://github.com/Kitware/CMake/releases/download/v3.23.3/cmake-3.23.3-linux-x86_64.sh \
    && bash /tmp/cmake.sh --skip-license --prefix=/opt/cmake --exclude-subdir \
    && rm /tmp/cmake.sh
ENV PATH="/opt/cmake/bin:${PATH}"

RUN git clone https://invent.kde.org/frameworks/extra-cmake-modules --branch kf5 \
    && cmake -B ecm-build -S extra-cmake-modules -DCMAKE_INSTALL_PREFIX=/opt/nativetooling \
    && cmake --build ecm-build --target install \
    && rm -r extra-cmake-modules && rm -r ecm-build

ENV QT_TAG v5.15.5-lts-lgpl

RUN cd && git clone https://invent.kde.org/qt/qt/qtbase.git --single-branch --branch ${QT_TAG} && cd qtbase \
    && ./configure -prefix /opt/nativetooling -opensource -confirm-license -no-gui -release -optimize-size -nomake tests -nomake examples -no-feature-concurrent \
    && make -j`nproc` && make install && rm -rf ~/qtbase

# QtQml Needed for native tooling (ki18n)
RUN cd && git clone https://invent.kde.org/qt/qt/qtdeclarative.git --single-branch --branch ${QT_TAG} && cd qtdeclarative \
    && QMAKESPEC=linux-g++ /opt/nativetooling/bin/qmake . && make -j`nproc` && make install && rm -rf ~/qtdeclarative

RUN git clone https://invent.kde.org/frameworks/kconfig --branch kf5 \
    && rm -rf kconfig/poqm \
    && cmake -B kconfig-build -S kconfig -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DBUILD_SHARED_LIBS=OFF -DBUILD_TESTING=OFF -DKCONFIG_USE_GUI=OFF \
    && cmake --build kconfig-build -t install \
    && rm -r kconfig && rm -r kconfig-build

RUN git clone https://invent.kde.org/frameworks/kcoreaddons --branch kf5 \
    && rm -rf kcoreaddons/poqm \
    && cmake -B kcoreaddons-build -S kcoreaddons -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DBUILD_SHARED_LIBS=OFF -DBUILD_TESTING=OFF \
    && cmake --build kcoreaddons-build -t install \
    && rm -r kcoreaddons && rm -r kcoreaddons-build

ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
ENV PERSIST=1

# setup craft
ADD CI-Craft-Deploy-Config.ini CI-Craft-Packages.shelf $HOME/

RUN git clone https://invent.kde.org/packaging/craftmaster \
    && git clone https://invent.kde.org/sysadmin/ci-utilities \
    && python craftmaster/CraftMaster.py --config ci-utilities/craft/qt5/CraftConfig.ini --config-override CI-Craft-Deploy-Config.ini --target android-arm-clang --setup \
    && python craftmaster/CraftMaster.py --config ci-utilities/craft/qt5/CraftConfig.ini --config-override CI-Craft-Deploy-Config.ini --target android-arm-clang -c -i craft \
    && python craftmaster/CraftMaster.py --config ci-utilities/craft/qt5/CraftConfig.ini --config-override CI-Craft-Deploy-Config.ini --target android-arm-clang -c --unshelve CI-Craft-Packages.shelf \
    && rm -r android-arm-clang/build && rm -r ci-utilities && rm -r craftmaster && rm -r blueprints && rm -r craft-clone && rm -r downloads && rm -r android-arm-clang/home && rm -r android-arm-clang/tmp
